package be.faros;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClimateWatchApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClimateWatchApplication.class, args);
    }
}
